<?php 


class automovil 
{

    /*
    $color;
    $marca;
    $modelo;
    $puertas;
    $placa;
    $descripcion;
    $imagen;*/

    public function __construct(){
        $this->conn = new mysqli('localhost', 'adrian', '123', 'mibasededatos');
    }

    public function getTodos(){
        $query = $this->conn->query('select * from automovil');
        $retorno = [];

        foreach ($query as $item){
            $retorno[] = $item;
        }

        return $retorno;
    }

    public function getAutomovilPorId($id){
        $query = $this->conn->query('select * from automovil where id='.$id);
        $retorno = [];

        foreach ($query as $item){
            $retorno[] = $item;
        }

        return $retorno[0];
    }

    public function DeleteAutomovil($id){
        return $this->conn->query('delete from automovil where id='.$id);

    }

    public function addAutomovil($datos){
            $query = $this->conn->query('insert into automovil 
            (color_id, marca_id, modelo_id, puertas, placa, descripcion, imagen_id) values 
            ('.$datos['color'].', '.$datos['marca'].', '.$datos['modelo'].', '.$datos['puertas'].', 
            "'.$datos['placa'].'", "'.$datos['descripcion'].'", '.$datos['imagen'].') ');

            return $query;
    }

    public function updateAutomovil($id, $data){
        $automovil = $this->getAutomovilPorId($id);
        if ($automovil['marca_id'] != $data['marca'])
            $set[] = 'marca_id ='.$data['marca'];
        if ($automovil['modelo_id'] != $data['modelo'])
            $set[] = 'modelo_id ='.$data['modelo'];
        if ($automovil['color_id'] != $data['color'])
            $set[] = 'color_id ='.$data['color'];
        if ($automovil['placa'] != $data['placa'])
            $set[] = 'placa ="'.$data['placa'].'"';
        if ($automovil['descripcion'] != $data['descripcion'])
            $set[] = 'descripcion ="'.$data['descripcion'].'"';
        if ($automovil['puertas'] != $data['puertas'])
            $set[] = 'puertas ="'.$data['puertas'].'"';
        if(isset($data['imagen'])){
            $set[] = 'imagen_id ='.$data['imagen'];
        }
        if (empty($set))
            return false;
        $sets = implode(', ', $set);
            
        $query = $this->conn->query('update automovil set '.$sets .' where id ='.$id);
    }

} 
?>