<?php 

class imagen {

    public function __construct(){
        $this->conn = new mysqli('localhost', 'adrian', '123', 'mibasededatos');
    }

    public function getTodos(){
        $query = $this->conn->query('select * from imagen');
        $retorno = [];

        foreach ($query as $item){
            $retorno[] = $item;
        }

        return $retorno;
    }

    public function getImagenPorId($id){
        $query = $this->conn->query('select * from imagen where id='.$id);
        $retorno = [];

        foreach ($query as $item){
            $retorno[] = $item;
        }

        return $retorno[0];
    }

    public function addImagen($data){
        $queryImagen = $this->conn->query('insert into imagen (nombre, extension, contenido) values ("'.$data['nombre'].'", "'.$data['extension'].'", "'.$this->conn->real_escape_string($data['contenido']).'") ');
        if($queryImagen){
            return $this->conn->insert_id;
        }
        return false;
    }

}

?>