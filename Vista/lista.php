<html>
    <head>
        <meta charset="utf-8"/>
        <meta name="author" content="Angel Villalobos" />
        <link href="public/css/miCss.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="alinear-centro">
            <div id="myModal" class="modal"></div>
            <h1>*** Lista de automoviles ***</h1>
                <table>
                    <thead>
                        <tr>
                            <td width = "5%">id</td>
                            <td width = "15%">marca</td>
                            <td width = "27%">modelo</td>
                            <td width = "8%">accion</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (isset($data)){ ?>
                            <?php foreach ($data as $dat){ ?> 
                                <tr>
                                    <td> <?php echo $dat['id'] ?> </td>
                                    <td> <?php echo $dat['marca']['nombre'] ?> </td>
                                    <td> <?php echo $dat['modelo']['nombre'] ?> </td>
                                    <td> 
                                        <a class="clickable" onclick="eliminar(<?php echo $dat['id'] ?>)" title="Eliminar">&xotime;</a> 
                                        <a  class="clickable" onclick="editar(<?php echo $dat['id'] ?>)" title="Editar">&Esim;</a>
                                        <a  class="clickable" onclick="detalle(<?php echo $dat['id'] ?>)" title="Detalle">&Xi;</a>
                                    </td>
                                </tr>
                            <?php } ?>
                        <?php } ?>
                    </tbody>
                </table>
        </div>
        <div class="alinear-derecha">
            <a class="clickable" onclick="agregar()">&xoplus;Agregar</a>
        </div>
        <script src="public/js/miJs.js"></script>
    </body>
    <footer>
        <p>&copy; Angel Villalobos 2020</p>
    </footer>
</html>