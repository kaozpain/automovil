<div class="modal-content">
    <span class="close clickable" onclick="cerrarModal()">&times;</span>
    <div>
        <form action="Controlador/editarAutomovil.php?id=<?php echo $data['id'] ?>" enctype="multipart/form-data" method="POST">
            <strong>Marca: </strong>
            <select name="marca"> 
                <?php foreach ($TodasMarcas as $marca) { ?>
                    <?php if ($data['marca']['id'] == $marca['id']) { ?>
                        <option value="<?php echo $marca['id'] ?>" selected><?php echo $marca['nombre'] ?></option>
                    <?php } else { ?>
                        <option value="<?php echo $marca['id'] ?>"><?php echo $marca['nombre'] ?></option>
                    <?php } ?>
                <?php } ?>
            </select>
            <br>
            <strong>Modelo: </strong>
            <select name="modelo"> 
                <?php foreach ($TodosModelos as $modelo) { ?>
                    <?php if ($data['modelo']['id'] == $modelo['id']) { ?>
                        <option value="<?php echo $modelo['id'] ?>" selected><?php echo $modelo['nombre'] ?></option>
                    <?php } else { ?>
                        <option value="<?php echo $modelo['id'] ?>"><?php echo $modelo['nombre'] ?></option>
                    <?php } ?>
                <?php } ?>
            </select>
            <br>
            <strong>Color: </strong>
            <select name="color"> 
                <?php foreach ($TodosColores as $color) { ?>
                    <?php if ($data['color']['id'] == $color['id']) { ?>
                        <option value="<?php echo $color['id'] ?>" selected><?php echo $color['nombre'] ?></option>
                    <?php } else { ?>
                        <option value="<?php echo $color['id'] ?>"><?php echo $color['nombre'] ?></option>
                    <?php } ?>
                <?php } ?>
            </select>
            <br>
            <strong>Puertas: </strong>
            <input type="text" name="puertas" value="<?php echo $data['puertas'] ?>" />
            <br>
            <strong>Placa: </strong>
            <input type="text" size="10" title="Max 10 caracteres" name="placa" value="<?php echo $data['placa'] ?>" />
            <br>
            <strong>Descripcion: </strong>
            <input type="text" size="50" title="Max 50 caracteres" name="descripcion" value="<?php echo $data['descripcion'] ?>" />
            <br>        
            <strong>Imagen: </strong>
            <input type="file" id="image" name="imagen" onchange="cambiarValorBandera()">
            <input type="hidden" id="imagenEscondidaBandera" name="imagenEscondidaBandera" value="<?php echo $data['imagen']['id'] ?>" >
            <br>
            <button type="submit">Guardar</button>

        </form>
    </div>
</div>