<div class="modal-content">
    <span class="close clickable" onclick="cerrarModal()">&times;</span>
    <div>
        <form method="POST" action="Controlador/agregarAutomovil.php" enctype="multipart/form-data">
            <strong>Marca: </strong>
            <select name="marca"> 
                <?php foreach ($TodasMarcas as $marca) { ?>
                    <option value="<?php echo $marca['id'] ?>"><?php echo $marca['nombre'] ?></option>
                <?php } ?>
            </select>
            <br>
            <strong>Modelo: </strong>
            <select name="modelo"> 
                <?php foreach ($TodosModelos as $modelo) { ?>
                    <option value="<?php echo $modelo['id'] ?>"><?php echo $modelo['nombre'] ?></option>
                <?php } ?>
            </select>
            <br>
            <strong>Color: </strong>
            <select name="color"> 
                <?php foreach ($TodosColores as $color) { ?>
                    <option value="<?php echo $color['id'] ?>"><?php echo $color['nombre'] ?></option>
                <?php } ?>
            </select>
            <br>
            <strong>Puertas: </strong>
            <input type="text" name="puertas" title="numero de puertas" required>
            <br>
            <strong>Placa: </strong>
            <input type="text" size="10" title="Max 10 caracteres" name="placa" title="placa" required>
            <br>
            <strong>Descripcion: </strong>
            <input type="text" size="50" title="Max 50 caracteres" name="descripcion" title="descripcion" required>
            <br>
            <strong>Imagen: </strong>
            <input type="file" id="image" name="imagen" required>
            <br>
            <button onclick="redireccionar()" type="submit">Guardar</button>
        </form>
    </div>
</div>