<?php 

require('Modelo/automovil.php');
require('Modelo/marca.php');
require('Modelo/modelo.php');

$automovil = new automovil();
$marca = new marca();
$modelo = new modelo();


$dataAutomovil = $automovil->getTodos();


foreach ($dataAutomovil as $datAutomovil){
    $data[$datAutomovil['id']]['id'] = $datAutomovil['id'];
    $data[$datAutomovil['id']]['marca'] = $marca->getMarcaPorId($datAutomovil['marca_id']);
    $data[$datAutomovil['id']]['modelo'] = $modelo->getModeloPorId($datAutomovil['modelo_id']);
}

require('Vista/lista.php');

?>