<?php 
if (isset($_POST)){
    require('../Modelo/automovil.php');

    $automovil = new automovil();

    $data = $_POST;
    if ($_FILES['imagen']){
        require('../Modelo/imagen.php');
        $imagen = new imagen();
        $revisar = getimagesize($_FILES["imagen"]["tmp_name"]);
        if($revisar !== false){
            $imagenData['nombre'] = $_FILES['imagen']['name'];
            $imagenData['extension'] = $_FILES['imagen']['type'];
            $imagenData['contenido'] = file_get_contents($_FILES['imagen']['tmp_name']);
            $data['imagen'] = $imagen->addImagen($imagenData);
        }
    }

    $automovil->addAutomovil($data);
    header('Location: /automovil');
}

?>