<?php 
if (isset($_GET['id'])){
    require('../Modelo/automovil.php');
    require('../Modelo/marca.php');
    require('../Modelo/modelo.php');
    require('../Modelo/color.php');

    $automovil = new automovil();
    $marca = new marca();
    $modelo = new modelo();
    $color = new color();


    $dataAutomovil = $automovil->getAutomovilPorId($_GET['id']);

    $data['id'] = $dataAutomovil['id'];
    $data['marca'] = $marca->getMarcaPorId($dataAutomovil['marca_id']);
    $data['modelo'] = $modelo->getModeloPorId($dataAutomovil['modelo_id']);
    $data['color'] = $color->getColorPorId($dataAutomovil['color_id']);
    $data['puertas'] = $dataAutomovil['puertas'];
    $data['placa'] = $dataAutomovil['placa'];
    $data['descripcion'] = $dataAutomovil['descripcion'];
    $data['imagenId'] = $dataAutomovil['imagen_id'];

    return require('../Vista/detalle.php');
}

?>