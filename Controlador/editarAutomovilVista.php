<?php 
if (isset($_GET['id'])){
    require('../Modelo/automovil.php');
    require('../Modelo/marca.php');
    require('../Modelo/modelo.php');
    require('../Modelo/color.php');
    require('../Modelo/imagen.php');

    $automovil = new automovil();
    $marca = new marca();
    $modelo = new modelo();
    $color = new color();
    $imagen = new imagen();


    $dataAutomovil = $automovil->getAutomovilPorId($_GET['id']);
    $TodasMarcas = $marca->getTodos();
    $TodosModelos = $modelo->getTodos();
    $TodosColores = $color->getTodos();

    $data['id'] = $dataAutomovil['id'];
    $data['marca'] = $marca->getMarcaPorId($dataAutomovil['marca_id']);
    $data['modelo'] = $modelo->getModeloPorId($dataAutomovil['modelo_id']);
    $data['color'] = $color->getColorPorId($dataAutomovil['color_id']);
    $data['puertas'] = $dataAutomovil['puertas'];
    $data['placa'] = $dataAutomovil['placa'];
    $data['descripcion'] = $dataAutomovil['descripcion'];
    $data['imagen'] = $imagen->getImagenPorId($dataAutomovil['imagen_id']);

    require('../Vista/editar.php');
}

?>