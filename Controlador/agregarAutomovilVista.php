<?php
require('../Modelo/marca.php');
require('../Modelo/modelo.php');
require('../Modelo/color.php');

$marca = new marca();
$modelo = new modelo();
$color = new color();

$TodasMarcas = $marca->getTodos();
$TodosModelos = $modelo->getTodos();
$TodosColores = $color->getTodos();

return require('../Vista/agregar.php');
?>