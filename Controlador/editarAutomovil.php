<?php 
if (isset($_POST) && isset($_GET['id'])){
    require('../Modelo/automovil.php');

    $automovil = new automovil();

    $data = $_POST;
    unset($data['imagen']);
    if ($_POST['imagenEscondidaBandera'] == 0 || $_POST['imagenEscondidaBandera'] == "0"){
        if ($_FILES['imagen']){
            require('../Modelo/imagen.php');
            $imagen = new imagen();
            $revisar = getimagesize($_FILES["imagen"]["tmp_name"]);
            if($revisar !== false){
                $imagenData['nombre'] = $_FILES['imagen']['name'];
                $imagenData['extension'] = $_FILES['imagen']['type'];
                $imagenData['contenido'] = file_get_contents($_FILES['imagen']['tmp_name']);
                $data['imagen'] = $imagen->addImagen($imagenData);
            }
        }
    }

    $automovil->updateAutomovil($_GET['id'], $data);
    header('Location: /automovil');
}

?>