function eliminar(id){
    var URLform = window.location+'/Controlador/eliminarAutomovil.php?id='+id;
    var req = new XMLHttpRequest();
    req.open('GET', URLform, true);
    req.onreadystatechange = function (aEvt) {
        if (req.readyState == 4) {
            if(req.status == 200)
                window.location.href = "/automovil";
            else
                dump("Error loading page\n");
        }
    };
    req.send();
}

function detalle(id){
    var URLform = window.location+'/Controlador/detalleAutomovilVista.php?id='+id;
    var req = new XMLHttpRequest();
    req.open('GET', URLform, true);
    req.onreadystatechange = function (aEvt) {
        if (req.readyState == 4) {
            if(req.status == 200){
                console.log(document.getElementById('myModal'));
                document.getElementById('myModal').innerHTML = req.responseText;
                document.getElementById('myModal').style.display = "block";
            }
            else
                dump("Error loading page\n");
        }
    };
    req.send();
}

function editar(id){
    var URLform = window.location+'/Controlador/editarAutomovilVista.php?id='+id;
    var req = new XMLHttpRequest();
    req.open('GET', URLform, true);
    req.onreadystatechange = function (aEvt) {
        if (req.readyState == 4) {
            if(req.status == 200){
                console.log(document.getElementById('myModal'));
                document.getElementById('myModal').innerHTML = req.responseText;
                document.getElementById('myModal').style.display = "block";
            }
            else
                dump("Error loading page\n");
        }
    };
    req.send();
}

function agregar(){
    var URLform = window.location+'/Controlador/agregarAutomovilVista.php';
    var req = new XMLHttpRequest();
    req.open('GET', URLform, true);
    req.onreadystatechange = function (aEvt) {
        if (req.readyState == 4) {
            if(req.status == 200){
                console.log(document.getElementById('myModal'));
                document.getElementById('myModal').innerHTML = req.responseText;
                document.getElementById('myModal').style.display = "block";
            }
            else
                dump("Error loading page\n");
        }
    };
    req.send();
}

function cerrarModal(){
    document.getElementById('myModal').style.display = "none";
}

function cambiarValorBandera(){
    document.getElementById('imagenEscondidaBandera').value = 0;
}

function mostrarPuertas(val) {
    document.getElementById('puertasShow').innerHTML=val; 
}
//https://www.w3schools.com/xml/ajax_xmlhttprequest_send.asp